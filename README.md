===================
PointFinder
===================

This project documents PointFinder service


Documentation
=============

## What is it?

The PointFinder service contains one python script *PointFinder.py* which is the script of the lates
version of the PointFinder service. The method detects chromosomal mutations predictive of drug resistance based on WGS data.

## Content of the repository
1. pointfinder.py     - the program
2. INSTALL_DB   - shell script for downloading the PointFinder database
3. UPDATE_DB    - shell script for updating the database to the newest version
4. VALIDATE_DB  - python script for verifying the database contains all
                  required files 
5. brew.sh      - shell script for installing dependencies
6. makefile     - make script for installing dependencies
7. test.fsa     - test fasta file
8. submodules	- directory with the blaster submodule required

## Installation

Setting up pPointFinder
```bash
# Go to wanted location for resfinder
cd /path/to/some/dir
# Clone and enter the pointfinder directory
git clone --recursive https://bitbucket.org/genomicepidemiology/pointfinder.git
cd pointfinder
```

Installing up the PointFinder database
```bash
cd /path/to/pointfinder
./INSTALL_DB database

# Check all DB scripts works, and validate the database is correct
./UPDATE_DB database
./VALIDATE_DB database
```

Installing dependencies:

Biopython: http://biopython.org/DIST/docs/install/Installation.html
Blastn: ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/

Blast will also be installed when running brew.sh if BlastN are not already installed and place in the user's path.
After running brew.sh and installing Blast add this command to the end of your ~/bash_profile to add BlastAll and FormatDB to the user's path

```bash
export PATH=$PATH:blast-2.2.26+/bin
```

If you want to download the two external tools from the Blast package, BlastN, yourself go to
```url
ftp://ftp.ncbi.nlm.nih.gov/blast/executables/release/LATEST
```

and download the version for your OS with the format:
```url
blast-version-architecture-OS.tar.gz
```

after unzipping the file, add this command to the end of your ~/bash_profile.
```bash
export PATH=$PATH:/path/to/blast-folder/bin
```

where path/to/blast-folder is the folder you unzipped.


The scripts are self contained. You just have to copy them to where they should
be used. Only the *database* folder needs to be updated mannually.

Remember to add the program to your system path if you want to be able to invoke the program without calling the full path.
If you don't do that you have to write the full path to the program when using it.

## Usage 

The program can be invoked with the -h option to get help and more information of the service.

```bash
Usage: python pointfinder.py [options]

Options:

    -h HELP
                    Prints a message with options and information to the screen
    -p DATABASE
                    The path to where you have located the database folder
    -b BLAST
                    The path to the location of blast-2.2.26 if it is not added
                    to the users path (see the install guide in 'README.md')
    -i INFILE
                    Your input file which needs to be preassembled partial
                    or complete genomes in fasta format
    -o OUTFOLDER
                    The folder you want to have your output files places.
                    If not specified the program will create a folder named
                    'Output' in which the result files will be stored.
    -s SPECIES
                    The PointFinder scheme you want to use. The options can be found in
                    the 'config' file in the database folder
    -u UNKNOWN
                    Incude if you want to have a report of all differences to the
                    reference gene including all that has not at this point been
                    associated with resistance (use '-u 1' to include this feature). 
```

#### Example of use with the *database* folder located in the current directory and Blast added to the user's path
```python
    python pointfinder.py -i test.fsa -o OUTFOLDER -s e.coli 
```
#### Example of use with the *database* and *blast-2.2.26* folders loacted in other directories
```python
    python pointfinder.py -p path/to/database -b path/to/blast-2.2.26+/bin -i test.fsa \
    -o OUTFOLDER -s e.coli
```

## Web-server

A webserver implementing the methods is available as a feature of the ResFinder service at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/ResFinder-3.0/


## The Latest Version


The latest version can be found at
https://bitbucket.org/genomicepidemiology/pointfinder/overview

## Documentation


The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/pointfinder/overview.


Citation
=======

When using the method please cite:

Will be added soon

License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
